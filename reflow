#!/usr/bin/python3
'''reflow RFC markdown for one line per sentence

Note that it is intended to focus on a very idiosycratic subset of
markdown for RFCs.  In particular:

- it expects the leading YAML block, but it ignores its content

- bulleted lists use '-' for bullets, and are indented by only two
  spaces per level

- all lists start at indent level 0

- headings are all prefixed by #, not underlined

- all tables begin with a clear {: title="foo"} marker

- bulleted lists aren't nested more than two levels deep

- definition lists aren't nested at all

- verbatim text is indicated by indent of four spaces, not delimited
  by header/footer lines

If your source doesn't (or can't) follow these conventions, you might
need to augment this tool.

This script operates on standard in and writes to stdout.

You can use it something like:

    reflow < xxx.md > xxx.reflowed.md
    make xxx.txt xxx.reflowed.txt
    diff xxx.txt xxx.reflowed.txt
    # if all looks reasonable:
    mv xxx.reflowed.md xxx.md
    git commit xxx.md -m 'reflowed text (no semantic changes)'
    

Author: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
License: CC0
Please feel free to reuse this and distribute it in any way you like.

'''


import re
import sys
from typing import List

def is_table(para:str) -> bool:
    return para.startswith('{: title="')

def is_verbatim(para:str) -> bool:
    for line in para.split('\n'):
        if not line.startswith('    '):
            return False
    return True

def is_yaml_metadata(para:str) -> bool:
    return para.startswith('---\n')

def is_quoted_text(para:str) -> bool:
    for line in para.split('\n'):
        if not line.startswith('>'):
            return False
    return True

def is_header(para:str) -> bool:
    if re.compile('^\#+ [^\n]*$').match(para):
        return True
    return False

def reflow(para:str) -> str:
    # don't reflow tables, verbatim blocks, YAML metadata, or quoted text:
    if is_table(para) or is_verbatim(para) or is_yaml_metadata(para) or is_quoted_text(para):
        return para

    if is_header(para):
        return para

    intro:str = ''
    lines:List[str] = para.split('\n')

    while lines[0].startswith('{:'):
        intro += lines[0] + '\n'
        lines = lines[1:]

    indent:str = ''

    # handle definition lists
    if len(lines) > 1 and lines[1].startswith(': '):
        intro += lines[0] + '\n'
        lines = lines[1:]
        indent = '  '
 
    if len(lines) > 1:
        match = re.compile('^ +').match(lines[1])
        if match:
            indent = match.group()

    # if a paragraph starts with some leading space, we shouldn't strip that
    leading_space = re.compile('^ +').match(lines[0])
    if leading_space:
        intro += leading_space.group()

    x = ' '.join(map(str.strip, lines))
    # assume all sentences split with a period (.), and not ! or ?
    # also, accept accidental breaks on abbreviations like John Q. Public
    sentences:List[str] = x.split('. ')

    # avoid splitting on a numbered list:
    list_number = re.compile('^[0-9]+$').match(sentences[0])
    if list_number:
        sentences = [sentences[0] + '. ' + sentences[1]] + sentences[2:]
        if indent == '':
            indent = ' '*(2 + len(list_number.group()))

    return intro + ('.\n' + indent).join(map(str.strip, sentences))

incoming = sys.stdin.read()

paras = incoming.strip().split('\n\n')

out = []

for para in paras:
    out += [reflow(para)]

print('\n\n'.join(out))
